from A import *

# def f(x):
#     return np.sin(x * x + np.exp(x));
# points = np.array([-1, -0.5, 0, 0.5, 1]);

def f(x):
    return 1 / (1 + 5 * x * x);
points = np.array([-5, -4, -3, -2, -1, 0, 1, 2, 3, 4, 5]);

if __name__ == "__main__":
    ps = np.array([points, f(points)])
    poly = calc_poly(ps);
    plot(ps, poly, f);
