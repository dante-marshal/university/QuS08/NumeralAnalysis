mod bisection;

fn target(x: f64) -> f64 {
    x.sin() + x + 0.1
}

fn main() {
    let range = (-1.0, 0.0);
    let tolerance = 0.00000000001;
    let result = bisection::run(target, range, tolerance);
    match result {
        Ok(x) => println!("x = {}, f(x) = {}", x, target(x)),
        Err(e) => println!("Error : {}", e),
    }
}
