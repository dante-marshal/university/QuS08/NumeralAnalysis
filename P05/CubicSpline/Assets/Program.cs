﻿using System;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.UI;

namespace TestMySpline
{
    class Program : MonoBehaviour
    {
        [SerializeField]
        private RectTransform target;
        [SerializeField]
        private float scale = 100f;
        [SerializeField]
        private Vector2[] points;
        [SerializeField, Min(1f)]
        private float animationDuration = 5f;
        [SerializeField, Range(1f, 3f)]
        private float displayRangeMult = 2.0f;
        [SerializeField]
        private float chScale1 = 500f, chScale2 = 100f;
        [SerializeField]
        private RawImage background;
        [SerializeField]
        private Text txtCoords;
        private CubicSpline spline;
        private (float min, float max) x_range, y_range;
        private (int min, int max) x_display, y_display;
        private void OnEnable()
        {
            Array.Sort<Vector2>(this.points, (a, b) => a.x.CompareTo(b.x));
            var xs = this.points.Convert(p => scale * p.x);
            var ys = this.points.Convert(p => scale * p.y);
            this.spline = new CubicSpline(xs, ys);
            this.x_range = (min: float.PositiveInfinity, max: float.NegativeInfinity);
            foreach (var x in xs)
            {
                if (x < x_range.min) x_range.min = x;
                if (x > x_range.max) x_range.max = x;
            }
            this.y_range = (min: float.PositiveInfinity, max: float.NegativeInfinity);
            foreach (var y in ys)
            {
                if (y < y_range.min) y_range.min = y;
                if (y > y_range.max) y_range.max = y;
            }
            x_display = (Mathf.CeilToInt(x_range.min * this.displayRangeMult), Mathf.FloorToInt(x_range.max * this.displayRangeMult));
            y_display = (Mathf.CeilToInt(y_range.min * this.displayRangeMult), Mathf.FloorToInt(y_range.max * this.displayRangeMult));
            this.background.texture = this.GenerateBackground();
        }
        private float animationTime;
        private void Update()
        {
            this.animationTime += Time.deltaTime;
            while (this.animationTime > this.animationDuration)
                this.animationTime -= this.animationDuration;
            var xn = this.animationTime / this.animationDuration;
            var x = Mathf.Lerp(x_range.min, x_range.max, xn);
            var y = this.spline.Eval(x)[0];
            this.txtCoords.text = $"({(x / scale):##0.0000}, {(y / scale):##0.0000})";
            var yn = Mathf.InverseLerp(y_display.min, y_display.max, y);
            xn = Mathf.InverseLerp(x_display.min, x_display.max, x);
            this.target.anchorMax = this.target.anchorMin = new Vector2(xn, yn);
        }
        private Texture GenerateBackground()
        {
            var size = (
                w: x_display.max - x_display.min + 1,
                h: y_display.max - y_display.min + 1
            );
            var cs = new Color[size.w * size.h];
            for (var yi = 0; yi < size.h; yi++)
            {
                var y = yi + y_display.min;
                for (var xi = 0; xi < size.w; xi++)
                {
                    var x = xi + x_display.min;
                    if (x > this.x_range.min && x < this.x_range.max && Mathf.Abs(this.spline.Eval(x)[0] - y) < 1f)
                        cs[yi * size.w + xi] = Color.green;
                    else if (x == 0 || y == 0)
                        cs[yi * size.w + xi] = Color.red;
                    else if (Mathf.Abs(x) > 5 && Mathf.Abs(y) > 5)
                        cs[yi * size.w + xi] = Color.black;
                    else if (((x % chScale1) == 0) || ((y % chScale1) == 0))
                        cs[yi * size.w + xi] = Color.blue;
                    else if (((x % chScale2) == 0) || ((y % chScale2) == 0))
                        cs[yi * size.w + xi] = Color.white;
                    else
                        cs[yi * size.w + xi] = Color.black;
                }
            };
            var ret = new Texture2D(size.w, size.h);
            ret.filterMode = FilterMode.Point;
            ret.SetPixels(cs);
            ret.Apply();
            return ret;
        }
    }
}
